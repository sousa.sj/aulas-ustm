package nossaempresa.com.xyz.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nossaempresa.com.xyz.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    //Lista a ser retornada no Construtor
    private String[] mDataset;

    //Referencia ao item da lista e trata de forma individual para a lista
    class MyViewHolder extends RecyclerView.ViewHolder {
        // Aqui chamam os componentes desenhados no Item de layout
        TextView textView;
        MyViewHolder(View v) {
            super(v);

            v = itemView;
            textView = (TextView) v.findViewById(R.id.info_text);

            //Metodo para colocar acção no item do RecyclerView
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    //Construtor que serve para sabermos que dados passar quando chamado na Activity.
    // Aqui especificamente, um array de String
    public MyAdapter(String[] myDataset) {
        mDataset = myDataset;
    }

    // Retorna o layout (item) que deve ser usado para a listagem no RecyclerView inflatado pelo View
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // Criar a instancia de layout para cada registo do RecyclerView
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pessoa, parent, false);

        //Chama aqui o seu ViewHolder e passa a View
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Método que recebe o ViewHolder e a posição da lista.
    // Retorna o objeto a ser apresentado pela posição e associado à ViewHolder.
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Associa os View definidos no ViewHolder aos Resultados que devem ser exibidos.
        holder.textView.setText(mDataset[position]);

    }

    // Retorna o numero de registos no RecyclerView (lista)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
