package nossaempresa.com.xyz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import nossaempresa.com.xyz.Adapter.AdapterWithObject;
//import nossaempresa.com.xyz.Adapter.MyAdapter;
import nossaempresa.com.xyz.Database.DatabaseAdapter;
import nossaempresa.com.xyz.Model.Pessoa;

public class SegundaActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    //private RecyclerView.Adapter mAdapter;
    private AdapterWithObject mAdapter;


    private List<Pessoa> pessoasList = new ArrayList<>();

    private double valor;
    //Lista de Nomes
    private String[] myDataset = new String[]{"Speia", "Jorge", "Dilson","Jaimito"};

    DatabaseAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        helper = new DatabaseAdapter(getApplicationContext());

        Intent i = this.getIntent();
        valor = i.getDoubleExtra("teste", 0);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        //Para manter o tamanho do RecyclerView
        recyclerView.setHasFixedSize(true);
        //Para mostrar divisoria nos items
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        //Mostra disposição dos elementos no RecyclerView
        layoutManager = new LinearLayoutManager(this);
        //Associar o tipo de layoutManager ao RecyclerView
        recyclerView.setLayoutManager(layoutManager);

        //Classe para Associar o conteudo para ao objecto por mostrar
        //mAdapter = new MyAdapter(myDataset);
        pessoasList.addAll(helper.getPersons());
        mAdapter = new AdapterWithObject(pessoasList);
        //Associar o adpter ao RecyclerView
        recyclerView.setAdapter(mAdapter);

        lerNomes();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, valor+"", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    private void lerNomes() {
        Pessoa pessoa = new Pessoa("Carla");
        pessoasList.add(pessoa);

        pessoa = new Pessoa("Joana");
        pessoasList.add(pessoa);

        pessoa = new Pessoa("Pedro");
        pessoasList.add(pessoa);

        mAdapter.notifyDataSetChanged();
    }

}
