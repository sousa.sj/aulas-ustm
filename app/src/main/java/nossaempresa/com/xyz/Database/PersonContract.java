package nossaempresa.com.xyz.Database;

import android.provider.BaseColumns;

public final class PersonContract {
    //Aqui definimos os nomes das tabelas e as respectivas colunas
    private PersonContract() {}

    public static class PersonEntry implements BaseColumns {
        public static final String TABLE_NAME = "person";
        public static final String COLUMN_NAME_NAME = "name";
    }
}
